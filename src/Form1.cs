using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NameMongkol
{
    public partial class Form1 : Form
    {
        string []m_character = { 
            "����������AIJQY", // �ҷԵ��
            "����骧��BKR", // �ѹ���
            "�����CLS", // �ѧ���
            "���ѭ����DMT", // �ظ
            "���˩ή̬�ENX", // �����
            "��Ǩ�FUVW", // �ء��
            "���׫�GOZ", // �����
            "������HP", // ����
            "����" // ࡵ�
        };

        public Form1()
        {
            InitializeComponent();
        }

        // ��Ǩ��Ҥ���ѡ�÷������ �դ���������Ţ��ʵ��
        private int chkPowerCharacter(char ch)
        {
            bool cmdExit = false;
            int line = 0;
            while (!cmdExit && line < m_character.Length)
            {
                if (m_character[line].IndexOf(ch) >= 0)
                {
                    return line + 1;
                }
                else
                {
                    line++;
                }
            }
            return 0;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int number = 0;
            if (textBox1.Text.Trim().Length > 0)
            {
                number = 0;
                textBox3.Text = "";
                for (int count = 0; count < textBox1.Text.Length; count++)
                {
                    int powerCharacter = chkPowerCharacter(textBox1.Text.ToUpper()[count]);
                    number += powerCharacter;
                    textBox2.Text = number.ToString();
                    textBox3.Text += powerCharacter.ToString() + "+";
                }
                textBox3.Text = textBox3.Text.Remove(textBox3.Text.Length - 1,1);
            }
            else
            {
                textBox2.Text = "";
                textBox3.Text = "";
            }
        }
    }
}